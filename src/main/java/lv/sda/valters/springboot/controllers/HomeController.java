package lv.sda.valters.springboot.controllers;

import lv.sda.valters.springboot.dto.SumForm;
import lv.sda.valters.springboot.repo.SumHistoryRepository;
import lv.sda.valters.springboot.service.CalculationService;
import lv.sda.valters.springboot.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Random;

@Controller
public class HomeController {
    private CalculationService calculationService;
    @Autowired
    private SumHistoryRepository sumHistoryRepository;
    @Autowired
    private CountryService countryService;

    public HomeController(CalculationService calculationService) {
        this.calculationService = calculationService;
    }

    @GetMapping("/")
    String home(Model model) {
        model.addAttribute("heading", "Hello World!");
        model.addAttribute("sumForm", new SumForm());
        return "index";
    }

    @GetMapping("/random")
    @ResponseBody
    String randomNumber() {
        return "<h2>Random number is " + new Random().nextInt(10) + "</h2>";
    }

    @GetMapping("/sum")
    @ResponseBody
    public String sum(@RequestParam Integer number1, @RequestParam Integer number2) {
        return String.valueOf(number1 + number2);
    }

    @PostMapping("/sum")
    public String postSum(@Valid SumForm form, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "index";
        }

        model.addAttribute("heading", "Sum");
        model.addAttribute("sum", calculationService.calculate(form));
        model.addAttribute("history", sumHistoryRepository.findAll());
        return "index";
    }

    @GetMapping("/multiply")
    @ResponseBody
    public String multiply(@RequestParam("number") Integer... numbers) {
        return String.valueOf(Arrays.stream(numbers).reduce(1, (a, b) -> a * b));
    }

    @GetMapping("/greeting/{languageIsoCode}")
    @ResponseBody
    public String getGreeting(@PathVariable String languageIsoCode) {
        switch (languageIsoCode) {
            case "lv":
                return "Sveika Pasaule!";
            case "ru":
                return "Privet Mir!";
        }
        return "Hello world!";
    }

    @GetMapping("/countries")
    public String getCountries(Model model) {
        model.addAttribute("countries", countryService.getAll());
        return "countries";
    }
}