package lv.sda.valters.springboot.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

public @Entity
class SumHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    private Integer number1;
    @NotNull
    private Integer number2;
    @NotNull
    private Integer sum;
    @NotNull
    private LocalDateTime time;

    public SumHistory() {
    }

    public SumHistory(@NotNull Integer number1, @NotNull Integer number2) {
        this.number1 = number1;
        this.number2 = number2;
        this.sum = number1 + number2;
        this.time = LocalDateTime.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumber1() {
        return number1;
    }

    public void setNumber1(Integer number1) {
        this.number1 = number1;
    }

    public Integer getNumber2() {
        return number2;
    }

    public void setNumber2(Integer number2) {
        this.number2 = number2;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SumHistory that = (SumHistory) o;
        return id.equals(that.id) &&
                number1.equals(that.number1) &&
                number2.equals(that.number2) &&
                sum.equals(that.sum) &&
                time.equals(that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number1, number2, sum, time);
    }

    @Override
    public String toString() {
        return "SumHistory{" +
                "id=" + id +
                ", number1=" + number1 +
                ", number2=" + number2 +
                ", sum=" + sum +
                ", time=" + time +
                '}';
    }
}