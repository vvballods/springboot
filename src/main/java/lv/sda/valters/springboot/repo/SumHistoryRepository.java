package lv.sda.valters.springboot.repo;

import lv.sda.valters.springboot.model.SumHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SumHistoryRepository extends CrudRepository<SumHistory, Long> {
}
