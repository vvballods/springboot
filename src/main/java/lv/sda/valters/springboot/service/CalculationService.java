package lv.sda.valters.springboot.service;

import lv.sda.valters.springboot.dto.SumForm;
import lv.sda.valters.springboot.model.SumHistory;
import lv.sda.valters.springboot.repo.SumHistoryRepository;
import org.springframework.stereotype.Service;

@Service
public class CalculationService {
    private SumHistoryRepository sumHistoryRepository;

    public CalculationService(SumHistoryRepository sumHistoryRepository) {
        this.sumHistoryRepository = sumHistoryRepository;
    }

    public Integer calculate(SumForm form) {
        return sumHistoryRepository.save(new SumHistory(form.getNumber1(), form.getNumber2())).getSum();
    }
}
