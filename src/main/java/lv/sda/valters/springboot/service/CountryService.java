package lv.sda.valters.springboot.service;

import lv.sda.valters.springboot.dto.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class CountryService {

    @Autowired
    private RestTemplate restTemplate;

    public List<Country> getAll() {
        return Arrays.asList(restTemplate.getForObject("https://restcountries.eu/rest/v2/all", Country[].class));
    }
}

