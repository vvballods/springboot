package lv.sda.valters.springboot;

import lv.sda.valters.springboot.dto.Country;
import lv.sda.valters.springboot.service.CountryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CountryServiceTest {
    @Autowired
    @InjectMocks
    private CountryService countryService;
    @Mock
    private RestTemplate restTemplate;

    @Test
    public void testCountriesEndpoint() {
        when(restTemplate.getForObject("https://restcountries.eu/rest/v2/all", Country[].class))
                .thenReturn(new Country[]{
                        new Country("Latvia")
                });

        assertThat(countryService.getAll()).isEmpty();
    }
}
