package lv.sda.valters.springboot;

import lv.sda.valters.springboot.repo.SumHistoryRepository;
import lv.sda.valters.springboot.service.CalculationService;
import lv.sda.valters.springboot.service.CountryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class HomeControllerTestWithoutServer {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private CalculationService calculationService;

    @SpyBean
    private SumHistoryRepository sumHistoryRepository;

    @MockBean
    private CountryService countryService;

    @MockBean
    private RestTemplate restTemplate;

    @Test
    public void testGreetingEnpoint() throws Exception {
        this.mvc.perform(get("/greeting/lv"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("Sveika Pasaule!")));
    }

}
